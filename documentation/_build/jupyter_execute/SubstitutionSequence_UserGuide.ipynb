{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# SubstitutionSequence, user guide\n",
    "\n",
    "The class `SubstitutionSequence` has been designed to work with sequence of `Substitution` objects. One knows that two `Substitution` applied in a raw do not necessarilly commute. The `SubstitutionSequence` deals with the permutation and combination of `Substitution` instances inside a sequence, in order to simplify, compress, and propose alternative understandings of the `Substitution` object, when taking together in a sequence.\n",
    "\n",
    "## Mathematical elaboration\n",
    "\n",
    "We start by elaborating notations and understanding of the `Substitution` object, seen as an operator from the ensemble of strings to the ensemble of strings. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Substitution operator\n",
    "\n",
    "The ensemble of string is denoted $\\Sigma$, and its elements are noted $\\sigma$. One can pick up a sub-part of a string, using Python notations. For instance, the part of the string $\\sigma$ from element $x_{1}$ to element $x_{2}$ is denoted $\\sigma\\left[x_{1}:x_{2}\\right]$. When either the first (starting) position or the second (ending) position is missing, they are replaced by $0$ or $\\left|\\sigma\\right|$, the length of the string. Recall that a sequence in Python starts from `0` position and that the last element is ommited. Contrary to the Python notation, the negative position are forbidden in our treatment, and we must have thus $0\\leq x_{1}\\leq x_{2}$ in the following, when discussing string (and subsequently, when discussing substitution operators).\n",
    "\n",
    "A substitution $S$ has three attributes: \n",
    " - the integer representing the start position, denoted $s$\n",
    " - the integer representing the end position, denoted $e$\n",
    " - the string $\\sigma$\n",
    " \n",
    "taken all together they form the substitution operator $S\\left(s,e,\\sigma\\right)$. When applied to a string $\\sigma_{0}$, one obtains the string $\\sigma_{1}$, defined as \n",
    "\n",
    "$$\n",
    "\\sigma_{1}=S\\left(s,e,\\sigma\\right)\\left[\\sigma_{0}\\right]=\\sigma_{0}\\left[:s\\right]+\\sigma+\\sigma_{0}\\left[e:\\right]\n",
    "$$\n",
    "\n",
    "where the $+$ operation between two strings represents the concatenation of the two strings (this also is a Python valid operation). Note that, in Python $\\sigma_{0}\\left[:i\\right]$ and $\\sigma_{0}\\left[i:\\right]$ are the empty string if $i>\\left|\\sigma_{0}\\right|$ ; this convention is also used in this document.\n",
    "\n",
    "There are two natural measures associated to a substitution: \n",
    " \n",
    " 1. The length of its substitution string $\\sigma$, that we note $\\left|\\sigma\\right|$\n",
    " 2. The length of the resulting string, once the substitution is performed. Without applying an explicit string, one cannot define this length, but we can define the *variation* of the length of the string due to the substitution. This is denoted as $\\left|S\\right| = s-e+\\left|\\sigma\\right|$. Note that $\\left|S\\right|$ is a variation of length, so it can be negative if the deleted part of the string is bigger than the inserted string.\n",
    "\n",
    "By construction, one has\n",
    "\n",
    "$$\n",
    "e+\\left|S\\right| = s+\\left|\\sigma\\right|\n",
    "$$\n",
    "\n",
    "so the two natural measures play a symmetric role with respect to the start/end position of the substitution.\n",
    "\n",
    "In the `Substitution` class, these two measures are denoted `len` and `abs`, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "9\n",
      "-3\n"
     ]
    }
   ],
   "source": [
    "from substitutionstring import Substitution\n",
    "\n",
    "substitution = Substitution(0,12,'123456789')\n",
    "print(len(substitution))\n",
    "print(abs(substitution))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inverse substitution\n",
    "\n",
    "The inverse operator $S^{-1}$ of the substitution $S\\left(s,e,\\sigma\\right)$ can only be defined by its action on a string. One defines \n",
    "\n",
    "\\begin{align}\n",
    "\\sigma_{1}=S\\left(s,e,\\sigma\\right)\\left[\\sigma_{0}\\right]\\Leftrightarrow\\sigma_{0}&=S^{-1}\\left(s,e,\\sigma\\right)\\left[\\sigma_{1}\\right]\\\\&=S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)\\left[\\sigma_{1}\\right]\n",
    "\\end{align}\n",
    "\n",
    "as can be checked by direct susbtitution of the definition of $S$. So the inverse substitution is itself a substitution, and records the part of the initial string $\\sigma_{0}$ that has been deleted during the substitution operation. \n",
    "\n",
    "The object $S^{-1}$ can be constructed by the method `Substitution.revert(string)`.\n",
    "\n",
    "If one elaborates a bit, one has \n",
    "\n",
    "\\begin{align}\n",
    "\\sigma_{0}&=S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)\\left[\\sigma_{1}\\right]\\\\&=S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)\\left[S\\left(s,e,\\sigma\\right)\\left[\\sigma_{0}\\right]\\right]\\\\&=S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)S\\left(s,e,\\sigma\\right)\\left[\\sigma_{0}\\right]\n",
    "\\end{align}\n",
    "\n",
    "which by no means signifies that $S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)S\\left(s,e,\\sigma\\right)$ is the identity operator, due to the presence of $\\sigma_{0}$ inside the first operator. In any case the application of a substitution requires a string onto which it applies.\n",
    "\n",
    "Let us check the above statement using a simple example. One simply replace $\\sigma_{0}$ by $\\sigma_{1}$ on the sequence $S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)S\\left(s,e,\\sigma\\right)$, that is we calculate $S\\left(s,s+\\left|\\sigma\\right|,\\sigma_{0}\\left[s:e\\right]\\right)S\\left(s,e,\\sigma\\right)\\left[\\sigma_{1}\\right]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'substitution teststration'"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sigma0 = 'substitution test'\n",
    "# substitute 'test'-> 'illustration'\n",
    "S = Substitution(13,17,'illustration')\n",
    "\n",
    "sigma1 = S.apply(sigma0)\n",
    "S_inverse = S.revert(sigma0)\n",
    "\n",
    "S_inverse.apply(S.apply(sigma1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is not the initial string, that we would recover when applying the sequence to `sigma0`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'substitution test'"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "S_inverse.apply(S.apply(sigma0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Two substitutions in a raw\n",
    "\n",
    "In general, two substitutions $S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)$ and $S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)$ do not commute, that is \n",
    "\n",
    "$$S_{1}S_{2}\\left[\\sigma\\right] \\neq S_{2}S_{1}\\left[\\sigma\\right]$$\n",
    "\n",
    "whatever their arguments. So a sequence of substitutions might be quite difficult to handle. Nevertheless, one can try to simplify such a sequence, simply because applying first $S_{1}$, and then $S_{2}$, the resulting substituted string may be a (perhaps complicated) concatenation of the two susbtituted string. One says that $S_{1}$ and $S_{2}$ overlap in such a situation. In that case, it becomes possible to replace the sequence of two substitutions by a single substitution, simplifying the complete sequence. \n",
    "\n",
    "Interestingly enough, the problem of overlapping substitution can be adressed without reference to the string onto which the substitution applies. In total, adding the cases where the two substitutions do not overlap, there are six situations of interest, namely\n",
    "\n",
    " 1. $s_{2}\\leq e_{2}<s_{1}\\leq e_{1}$: in this case $S_{2}$ is entirely on the left of $S_{1}$. This situations is refered as $S_{2}<S_{1}$.\n",
    " 2. $s_{1}\\leq e_{1} + \\left|\\sigma_{1}\\right| < s_{2} \\leq e_{2}$: in this case $S_{2}$ is entirely on the right of $S_{1}$. This situation is refered as $S_{2}>S_{1}$.\n",
    " 3. $s_{2} < s_{1} \\leq e_{1} + \\left|\\sigma_{1}\\right| < e_{2}$: in this case $S_{2}$ is entirely covering $S_{1}$ (said differently, the string $\\sigma_{2}$ of the second substitution will entirely erase the string $\\sigma_{1}$ of the first substitution)\n",
    " 4. $s_{2} < s_{1} \\leq e_{2} \\leq e_{1} + \\left|\\sigma_{1}\\right|$: in this case $S_{2}$ is partly covering $S_{1}$ on its left part (said differenly, the right part of the substituted string of $S_{2}$ overlaps with the left part of the substituted string of $S_{1}$)\n",
    " 5. $s_{1} \\leq s_{2} \\leq e_{2} < e_{1} + \\left|\\sigma_{1}\\right|$: in this case $S_{2}$ is entirely contained in $S_{1}$ (said differently the string $\\sigma_{2}$ will erase a part of $\\sigma_{1}$ at the end)\n",
    " 6. $s_{1} \\leq s_{2} \\leq e_{1} + \\left|\\sigma_{1}\\right| \\leq e_{2}$: in this case $S_{2}$ is partly covering $S_{1}$ on its right part (said differently the resulting substituted string will be the begining of $\\sigma_{1}$ concantenated with the entire $\\sigma_{2}$\n",
    "\n",
    "For the above cases to be correct, $S_{1}$ must be applied before $S_{2}$. One can verify that these six situations are the only possible situations available for two consecutive substitutions applied in a sequence. Note that one can exchange the $\\leq$ with some stricter $<$ in due places, without changing the construction of the sequence (one has still to be consistent, and be sure that any 5 parameters $s_{1},e_{1},\\left|\\sigma_{1}\\right|s_{2},e_{2}$ fall in one and only one situation described above.\n",
    "\n",
    "The two first cases correspond to the situations without overlap. In that case, there is no combination of the two substitutions, but we can still apply the transformations\n",
    " \n",
    " 1. $S_{1}>S_{2}\\Rightarrow S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S_{1}\\left(s_{1}+\\left|S_{2}\\right|,e_{1}+\\left|S_{2}\\right|,\\sigma_{1}\\right)S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)$\n",
    " 2. $S_{1}<S_{2}\\Rightarrow S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)S_{2}\\left(s_{2}-\\left|S_{1}\\right|,e_{2}-\\left|S_{1}\\right|,\\sigma_{2}\\right)$\n",
    " \n",
    "The last four situations correspond to overlapping substitutions. They can be combined in the following way\n",
    "\n",
    " 3. $S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S\\left(s_{2},e_{2}-\\left|S_{1}\\right|,\\sigma_{2}\\right)$ if $s_{2} < s_{1} \\leq \\tilde{e}_{1} < e_{2}$\n",
    " 4. $S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S\\left(s_{2},e_{1},\\sigma_{2}+\\sigma_{1}\\left[e_{2}-s_{1}:\\right]\\right)$ if $s_{2} < s_{1} \\leq e_{2} \\leq \\tilde{e}_{1}$\n",
    " 5. $S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S\\left(s_{1},e_{1},\\sigma_{1}\\left[:s_{2}-s_{1}\\right]+\\sigma_{2}+\\sigma_{1}\\left[e_{2}-s_{1}:\\right]\\right)$ if $s_{1} \\leq s_{2} \\leq e_{2} < \\tilde{e}_{1}$\n",
    " 6. $S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S\\left(s_{1},e_{2}-\\left|S_{1}\\right|,\\sigma_{1}\\left[:s_{2}-s_{1}\\right]+\\sigma_{2}\\right)$ if $s_{1} \\leq s_{2} \\leq \\tilde{e}_{1} \\leq e_{2}$\n",
    " \n",
    "where $\\tilde{e}_{1}=s_{1}+\\left|\\sigma_{1}\\right|=e_{1}+\\left|S_{1}\\right|$.\n",
    "\n",
    "After a few algebras, one can show that these four situations can be described by a unique combinations: \n",
    "\n",
    "\\begin{multline}\n",
    "s_{1}<s_{2}\\leq\\tilde{e}_{1}\\cup s_{2}\\leq s_{1}\\leq e_{2}\\Rightarrow\\\\S_{2}\\left(s_{2},e_{2},\\sigma_{2}\\right)S_{1}\\left(s_{1},e_{1},\\sigma_{1}\\right)=S\\left(s,e,\\sigma_{1}\\left[:s^{\\prime}\\right]+\\sigma_{2}+\\sigma_{1}\\left[e^{\\prime}:\\right]\\right)\n",
    "\\end{multline}\n",
    "\n",
    "with \n",
    "\n",
    "\\begin{align}\n",
    "s&=\\min\\left(s_{1},s_{2}\\right)\\\\e&=\\max\\left(\\tilde{e}_{1},e_{2}\\right)-\\left|S_{1}\\right|\\\\s^{\\prime}&=\\max\\left(s_{2}-s_{1},0\\right)\\\\e^{\\prime}&=\\min\\left(e_{2}-s_{1},\\left|\\sigma_{1}\\right|\\right)\n",
    "\\end{align}\n",
    "\n",
    "The two situations $s_{1}<s_{2}\\leq\\tilde{e}_{1}$ and $s_{2}\\leq s_{1}\\leq e_{2}$ are called `<=` ($S_{1}$ overlaping on the left of $S_{2}$) and `>=` ($S_{1}$ overlaping on the right of $S_{2}$) in term of the `Substitution` object. The four situations `>`, `<`, `<=` and `>=` completely determine the different possibilities for two consecutive `Substitution` objects to be ordered.\n",
    "\n",
    "Now we have all the algebra to combine a sequence, and to try to reduce it. The strategy is to capture the left-most substitution, supposed to be the one that has the minimum $s$ in the sequence, and to displace it at the begining of the sequence, permuting it with the non-overlaping substitution and combining it with the overlaping substitution during the displacement. That's all, and that's what the `SubstitutionSequence.compress` does. Nevertheless, du to the permutation of the objects without overlaping, it might be that the so-sorted `Substitution` are not in increasing order of their `start` position. So one has to recursively applies the `compress` method in order to be sure one has an ordered sequence at the end. In the same way, the last displacement of the `SubstitutionSequence.displace(start,end)` does not verify that the displaced element is not near a `Substitution` (already ordered) that still overlaps with it. So, one has to check this, do a displacement again, and launch again a compress method to verify all the machinery has been performed. At the end, this is not really costy in terms of recursivity.\n",
    "\n",
    "**Important remark:** while it is custom in mathematics to put the first applied operator on the right of the sequence, Python reads sequence from left to right. So the first applied `Substitution` in a `SubstitutionSequence` is in fact the zero-th element `SubstitutionSequence[0]`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Verification of the `sort` method\n",
    "\n",
    "To verify the compress method, one simply check that a randomly generated sequence of `Substitution`, once compressed, still transform a random sequence in the same way as its uncompressed version.\n",
    "\n",
    "A entire sequence can be applied at once using the `SubstitutionSequence.apply(string)` method. In addition, one can ask for the method `SubstitutionSequence.are_overlaping(p,q)` with two consecutive integers `p` and `q` representing the position of the `Substitution` in the `SubstitutionSequence`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "no assert failed\n"
     ]
    }
   ],
   "source": [
    "import random as r\n",
    "\n",
    "from substitutionstring.substitution_sequence import SubstitutionSequence\n",
    "\n",
    "def generate_long_sequence(N):\n",
    "    string = \"abcdefghijklmnopqrstuvwxyz0123456789\"\n",
    "    initial_string = ''.join(r.choices(string,k=550))\n",
    "    sub_string, sequence = initial_string, SubstitutionSequence()\n",
    "    for _ in range(N):\n",
    "        start, end = r.randrange(0,len(sub_string)), r.randrange(0,len(sub_string))\n",
    "        string_ = ''.join(r.choices(string,k=start))\n",
    "        if end <= start: \n",
    "            start, end = end, start\n",
    "        substitution = Substitution(start,end,string_)\n",
    "        sequence.append(substitution)\n",
    "        sub_string = substitution.apply(sub_string)\n",
    "    return sequence, initial_string\n",
    "\n",
    "# generate random sequence with N Substitution in it\n",
    "for N in range(15,25):\n",
    "    sequence, initial_string = generate_long_sequence(N)\n",
    "    string1 = sequence.apply(initial_string)\n",
    "    reordered_sequence = sequence.sort()\n",
    "    string2 = reordered_sequence.apply(initial_string)\n",
    "    # verify that the two SubstitutionSequence give the same string\n",
    "    assert(string1==string2)\n",
    "    # verify that the sorted SubstitutionSequence is indeed sorted\n",
    "    for subst1,subst2 in zip(reordered_sequence[:-1],reordered_sequence[1:]):\n",
    "        assert(subst1.start < subst2.start)\n",
    "    # verify there is no overlapping Substitution anymore\n",
    "    overlapping = [reordered_sequence.are_overlapping(p,p+1)\n",
    "                   for p in range(len(reordered_sequence)-1)]\n",
    "    assert(not any(overlapping))\n",
    "print(\"no assert failed\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Tue Aug  3 16:16:10 2021\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}