#!/usr/bin/env python
# coding: utf-8

# # SubstitutionString, user guide
# 
# `SubstitutionString` allows to transform any string into an other one without destroying information. By *not destroying information*, we mean that one can apply any transformation to a string (insertion, deletion, substitution, or event apply any REGular EXpression (see the documentation of the [re](https://docs.python.org/3/library/re.html) module of the Pyhton standard library for more details about REGEX) to substitute part of a string with an other one), and still be able to know which part has been transformed into the final version of the string.
# 
# ## A basic example
# 
# To illustrate the interest of the `SubstitutionString` class, let us suppose that one has a noisy text that someone wrote by hand, with inevitable mistake. We thus first would like to clean this text, for instance using REGEX. Then, suppose further that one does some detection (say by keyword, to simplify) and that one detects a token (= a sub-part with specified start and end position in the string) in the clean string. We would like to know what is the part of the initial noisy string that corresponds to our detection, since the original document is still the only one the human knows, and we would like to tag the original document, not the clean one. This situation often appears when one wants to construct Gold-Standard of annotated documents for instance.
# 
# Let us construct an explicit easy example of this situation. The noisy string will just have numbers (for easinness) in addition to the complete message. Then, one feeds the noisy string to the `SubstitutionString`, and one applies the REGEX substitution of digits by empty space. A word is badly written, so we replace it by its correct version. Then we make our detection: we ask ourselve *where is the object?*, so we detect some some text after some preposition `['in', 'near']` (keep things simple, still :-). now the question is: *how much the information was hidden from the rest of the string*. We perform some calculation about that. All in a fex lines

# In[1]:


import re
from substitutionstring import SubstitutionString


text = "I'v45e8 87hi54dd654en21 a54 s9t98ri654n3g"
text += " i321n 66a654 8n88o6i662s1y3 c54h653n1n58el"

sstring = SubstitutionString(text)
sstring.sub('\d','')
# the substitution performs inplace
print("Clean string")
print(str(sstring))
# capture some token
token_match = [(m.start(),m.end()) for m in re.finditer('\W(in|near)\W',str(sstring))]

print('-'*12)
print("captured token")
for match in token_match:
    print(str(sstring)[match[0]:match[1]])
    
# take the end of the sentence
print("we are interesting in the token")
print(str(sstring)[token_match[-1][1]:])

# correct misspeled word
print('-'*12)
sstring.sub('chnnel','channel')
print("after correction")
print(sstring.string)

# restore the token in its initial state
string, start, end = sstring.restore(start=token_match[-1][-1],end=len(sstring))

# the noisy part of the interesting message was
print('-'*12)
print("the found token was initially from position {} to {} in the initial text".format(start,end))
print("and corresponds to '"+string[start:end]+"' there")


# Some comments about the code. 
# 
# The access to the string at the present state of the `SubstitutionString` can be done in two ways: 
#  - `SubstitutionString.string` attribute
#  - `str(SubstitutionString)` method
# 
# Both returns the same string.
# 
# The method `Substitution.sub(regex_expression_to_find, substitution_string, flag=0)` accepts any REGEX in its first argument, and a string that will replace the REGEX as its second argument. The last argument is for flags, and accepts any flags that [re module](https://docs.python.org/3/library/re.html) accepts (recall that flags are separated by pipe `'|'` symbol, e.g. `flag=re.IGNORECASE|re.DOTALL` to ignore upper-/lower-case and to suppose that the `'.'` joker corresponds to any character in the REGEX. One has to import `re` if one needs to construct such flag. We do not need `re` to use the flag by default (it is `flag=0` ,and corresponds to no flag at all).
# 
# In addition to `SubstitutionString.sub()`, there are three more basic methods : 
#  - `SubstitutionString.insert(start, string)`: insert the `string` at position `start` of the actual state of the `SubstitutionString`.
#  - `SubstitutionString.delete(start, end)`: delete the slice `[start:end]` from the actual state of the `SubstitutionString`.
#  - `SubstitutionString.substitute(start, end, string)`: delete from `start` to `end` and insert at position `start` from the actual state of the `SubstitutionString`.

# ## `SubstitutionString.sequence` and the notion of *actual state*
# 
# How does `SubstitutionString` works under the hood ? Each time a transformation appears, it records a `Substitution` object into a `SubstitutionSequence` that one can export from the attribute `SubstitutionString.sequence`. this `Substitution` object corresponds to the substitution that one would perform to recover the previous state of the `SubstitutionString`.
# 
# In our example, at the end, we have quite a long sequence of `Substitution`, we show only the first 5 ones below. The number of `Substitution` in the `SubstitutionString.sequence` is given by the `len(SubstitutionString)` method.

# In[2]:


for substitution in sstring.sequence[:5]:
    print(substitution)
print('.')
print('.')
print('.')
print("number of Substitution in the sequence = " + str(len(sstring)))


# One sees that the `SubstitutionString.sequence` attribute presents only insertions. This is because our filtering procedure performed only deletion: we deleted all the digits. The first line above just tells us that the first step was to delete the string `'4'` at position `3`. Since we record the inverse of the `Substitution` that has been performed during the transformation, one sees an insertion of the string `'4'` at position `3`. To know which `Substitution` was performed during the `SubstitutionString.sub` method, one can call the reverse of the sequence. It is constructed from the `reversed(SubstitutionString)` method. 
# 
# *Note that* `reversed` returns an iterator, which can not be suscriptable. Since we are interested only in the first few `Substitution`, we force the construction of the entire reversed sequence using the `list(reversed(SubstitutionString))` trick, we transform the iterator into a list, which is iterable (this is also because we want to reuse the sequence later, whereas an iterator is empty once exhausted ; that `reversed` returns an iterator seems to be the standard behavior of this magic function in Python, so we comply to the standard).
# 
# *Note also* that `reversed(SubstitutionString)` is *not* `reversed(SubstitutionString.sequence)`. Said differently, `reversed(SubstitutionString)` is not the simple reversal of the list of `Substitution` (as `reversed(SubstitutionString.sequence)` would be), each `Substitution` is also changed to its inverse in the process of reversing the sequence.

# In[3]:


sequence = list(reversed(sstring))
for substitution in sequence[:5]:
    print(substitution)


# So our guess was correct, the first step (and all the other one from the `sub` example above) was a deletion.
# 
# One can perform the consecutive `Substitution` by hand to understand a bit what we call the *state of the `SubstitutionString`*. We start with the initial string

# In[4]:


string0 = text
print(string0)
string1 = sequence[0].apply(string0)
print(string1)


# We have one more time that the first `Substitution` deletes the first digit appearing in the initial string. One calls `string0` the initial state of the `SubstitutionString`, and `string1` the first state of the `SubstitutionString` instance. In addition, a `SubstitutionString` changes its state by the application of a `Substitution` operation.
# 
# The final state of the `SubstitutionString` is the one once all the `Substitution` have been performed (in fact, one can continue to transform the `SubstitutionString`, such that the *final* state has nothing definitive in principle).

# In[5]:


string_ = text
for substitution in sequence:
    string_ = substitution.apply(string_)
print(string_)
print(str(sstring))


# In fact we do not need to worry much about all this vocabulary, since we will only consider the final state of the `SubstitutionString` in general.

# ## `SubstitutionString.revert`
# 
# In any case, one can also apply the `revert` method to revert to a previous state of the `SubstitutionString`. Be warn that this method works in-place, and erase the last `Substitution` from the `SubstitutionString.sequence`

# In[6]:


string_1 = sstring.revert()
print(string_1)
print(len(sstring))


# One reverted to the previous state of the `SubstitutionString`, before the substitution of `'chnnel'` by `'channel'`, as one can see. At the same time, the `SubstitutionString.sequence` lost one `Substitution` element. 

# ## What does `SubstitutionString.restore(start, end)` do?
# 
# The `revert` method is not used to restore the initial state of the string. In fact, what `restore` does is to shift the `start` and `end` argument back to the initial state, and perform the inverse `Substitution` that `SubstitutionString.sequence` stores in order to bring back the initial state.
# 
# The way the positins are shifted is encoded in the `Substitution.shift_start_end(start, end)` method.

# In[7]:


from datetime import datetime
print("Last modification {}".format(datetime.now().strftime("%c")))

