#!/usr/bin/env python
# coding: utf-8

# # SubstitutionSequence, user guide
# 
# The class `SubstitutionSequence` has been designed to work with sequence of `Substitution` objects. One knows that two `Substitution` applied in a raw do not necessarilly commute. The `SubstitutionSequence` deals with the permutation and combination of `Substitution` instances inside a sequence, in order to simplify, compress, and propose alternative understandings of the `Substitution` object, when taking together in a sequence.
# 
# ## Mathematical elaboration
# 
# We start by elaborating notations and understanding of the `Substitution` object, seen as an operator from the ensemble of strings to the ensemble of strings. 

# ### Substitution operator
# 
# The ensemble of string is denoted $\Sigma$, and its elements are noted $\sigma$. One can pick up a sub-part of a string, using Python notations. For instance, the part of the string $\sigma$ from element $x_{1}$ to element $x_{2}$ is denoted $\sigma\left[x_{1}:x_{2}\right]$. When either the first (starting) position or the second (ending) position is missing, they are replaced by $0$ or $\left|\sigma\right|$, the length of the string. Recall that a sequence in Python starts from `0` position and that the last element is ommited. Contrary to the Python notation, the negative position are forbidden in our treatment, and we must have thus $0\leq x_{1}\leq x_{2}$ in the following, when discussing string (and subsequently, when discussing substitution operators).
# 
# A substitution $S$ has three attributes: 
#  - the integer representing the start position, denoted $s$
#  - the integer representing the end position, denoted $e$
#  - the string $\sigma$
#  
# taken all together they form the substitution operator $S\left(s,e,\sigma\right)$. When applied to a string $\sigma_{0}$, one obtains the string $\sigma_{1}$, defined as 
# 
# $$
# \sigma_{1}=S\left(s,e,\sigma\right)\left[\sigma_{0}\right]=\sigma_{0}\left[:s\right]+\sigma+\sigma_{0}\left[e:\right]
# $$
# 
# where the $+$ operation between two strings represents the concatenation of the two strings (this also is a Python valid operation). Note that, in Python $\sigma_{0}\left[:i\right]$ and $\sigma_{0}\left[i:\right]$ are the empty string if $i>\left|\sigma_{0}\right|$ ; this convention is also used in this document.
# 
# There are two natural measures associated to a substitution: 
#  
#  1. The length of its substitution string $\sigma$, that we note $\left|\sigma\right|$
#  2. The length of the resulting string, once the substitution is performed. Without applying an explicit string, one cannot define this length, but we can define the *variation* of the length of the string due to the substitution. This is denoted as $\left|S\right| = s-e+\left|\sigma\right|$. Note that $\left|S\right|$ is a variation of length, so it can be negative if the deleted part of the string is bigger than the inserted string.
# 
# By construction, one has
# 
# $$
# e+\left|S\right| = s+\left|\sigma\right|
# $$
# 
# so the two natural measures play a symmetric role with respect to the start/end position of the substitution.
# 
# In the `Substitution` class, these two measures are denoted `len` and `abs`, respectively.

# In[1]:


from substitutionstring import Substitution

substitution = Substitution(0,12,'123456789')
print(len(substitution))
print(abs(substitution))


# ### Inverse substitution
# 
# The inverse operator $S^{-1}$ of the substitution $S\left(s,e,\sigma\right)$ can only be defined by its action on a string. One defines 
# 
# \begin{align}
# \sigma_{1}=S\left(s,e,\sigma\right)\left[\sigma_{0}\right]\Leftrightarrow\sigma_{0}&=S^{-1}\left(s,e,\sigma\right)\left[\sigma_{1}\right]\\&=S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)\left[\sigma_{1}\right]
# \end{align}
# 
# as can be checked by direct susbtitution of the definition of $S$. So the inverse substitution is itself a substitution, and records the part of the initial string $\sigma_{0}$ that has been deleted during the substitution operation. 
# 
# The object $S^{-1}$ can be constructed by the method `Substitution.revert(string)`.
# 
# If one elaborates a bit, one has 
# 
# \begin{align}
# \sigma_{0}&=S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)\left[\sigma_{1}\right]\\&=S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)\left[S\left(s,e,\sigma\right)\left[\sigma_{0}\right]\right]\\&=S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)S\left(s,e,\sigma\right)\left[\sigma_{0}\right]
# \end{align}
# 
# which by no means signifies that $S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)S\left(s,e,\sigma\right)$ is the identity operator, due to the presence of $\sigma_{0}$ inside the first operator. In any case the application of a substitution requires a string onto which it applies.
# 
# Let us check the above statement using a simple example. One simply replace $\sigma_{0}$ by $\sigma_{1}$ on the sequence $S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)S\left(s,e,\sigma\right)$, that is we calculate $S\left(s,s+\left|\sigma\right|,\sigma_{0}\left[s:e\right]\right)S\left(s,e,\sigma\right)\left[\sigma_{1}\right]$.

# In[2]:


sigma0 = 'substitution test'
# substitute 'test'-> 'illustration'
S = Substitution(13,17,'illustration')

sigma1 = S.apply(sigma0)
S_inverse = S.revert(sigma0)

S_inverse.apply(S.apply(sigma1))


# This is not the initial string, that we would recover when applying the sequence to `sigma0`.

# In[3]:


S_inverse.apply(S.apply(sigma0))


# ### Two substitutions in a raw
# 
# In general, two substitutions $S_{1}\left(s_{1},e_{1},\sigma_{1}\right)$ and $S_{2}\left(s_{2},e_{2},\sigma_{2}\right)$ do not commute, that is 
# 
# $$S_{1}S_{2}\left[\sigma\right] \neq S_{2}S_{1}\left[\sigma\right]$$
# 
# whatever their arguments. So a sequence of substitutions might be quite difficult to handle. Nevertheless, one can try to simplify such a sequence, simply because applying first $S_{1}$, and then $S_{2}$, the resulting substituted string may be a (perhaps complicated) concatenation of the two susbtituted string. One says that $S_{1}$ and $S_{2}$ overlap in such a situation. In that case, it becomes possible to replace the sequence of two substitutions by a single substitution, simplifying the complete sequence. 
# 
# Interestingly enough, the problem of overlapping substitution can be adressed without reference to the string onto which the substitution applies. In total, adding the cases where the two substitutions do not overlap, there are six situations of interest, namely
# 
#  1. $s_{2}\leq e_{2}<s_{1}\leq e_{1}$: in this case $S_{2}$ is entirely on the left of $S_{1}$. This situations is refered as $S_{2}<S_{1}$.
#  2. $s_{1}\leq e_{1} + \left|\sigma_{1}\right| < s_{2} \leq e_{2}$: in this case $S_{2}$ is entirely on the right of $S_{1}$. This situation is refered as $S_{2}>S_{1}$.
#  3. $s_{2} < s_{1} \leq e_{1} + \left|\sigma_{1}\right| < e_{2}$: in this case $S_{2}$ is entirely covering $S_{1}$ (said differently, the string $\sigma_{2}$ of the second substitution will entirely erase the string $\sigma_{1}$ of the first substitution)
#  4. $s_{2} < s_{1} \leq e_{2} \leq e_{1} + \left|\sigma_{1}\right|$: in this case $S_{2}$ is partly covering $S_{1}$ on its left part (said differenly, the right part of the substituted string of $S_{2}$ overlaps with the left part of the substituted string of $S_{1}$)
#  5. $s_{1} \leq s_{2} \leq e_{2} < e_{1} + \left|\sigma_{1}\right|$: in this case $S_{2}$ is entirely contained in $S_{1}$ (said differently the string $\sigma_{2}$ will erase a part of $\sigma_{1}$ at the end)
#  6. $s_{1} \leq s_{2} \leq e_{1} + \left|\sigma_{1}\right| \leq e_{2}$: in this case $S_{2}$ is partly covering $S_{1}$ on its right part (said differently the resulting substituted string will be the begining of $\sigma_{1}$ concantenated with the entire $\sigma_{2}$
# 
# For the above cases to be correct, $S_{1}$ must be applied before $S_{2}$. One can verify that these six situations are the only possible situations available for two consecutive substitutions applied in a sequence. Note that one can exchange the $\leq$ with some stricter $<$ in due places, without changing the construction of the sequence (one has still to be consistent, and be sure that any 5 parameters $s_{1},e_{1},\left|\sigma_{1}\right|s_{2},e_{2}$ fall in one and only one situation described above.
# 
# The two first cases correspond to the situations without overlap. In that case, there is no combination of the two substitutions, but we can still apply the transformations
#  
#  1. $S_{1}>S_{2}\Rightarrow S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S_{1}\left(s_{1}+\left|S_{2}\right|,e_{1}+\left|S_{2}\right|,\sigma_{1}\right)S_{2}\left(s_{2},e_{2},\sigma_{2}\right)$
#  2. $S_{1}<S_{2}\Rightarrow S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S_{1}\left(s_{1},e_{1},\sigma_{1}\right)S_{2}\left(s_{2}-\left|S_{1}\right|,e_{2}-\left|S_{1}\right|,\sigma_{2}\right)$
#  
# The last four situations correspond to overlapping substitutions. They can be combined in the following way
# 
#  3. $S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S\left(s_{2},e_{2}-\left|S_{1}\right|,\sigma_{2}\right)$ if $s_{2} < s_{1} \leq \tilde{e}_{1} < e_{2}$
#  4. $S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S\left(s_{2},e_{1},\sigma_{2}+\sigma_{1}\left[e_{2}-s_{1}:\right]\right)$ if $s_{2} < s_{1} \leq e_{2} \leq \tilde{e}_{1}$
#  5. $S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S\left(s_{1},e_{1},\sigma_{1}\left[:s_{2}-s_{1}\right]+\sigma_{2}+\sigma_{1}\left[e_{2}-s_{1}:\right]\right)$ if $s_{1} \leq s_{2} \leq e_{2} < \tilde{e}_{1}$
#  6. $S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S\left(s_{1},e_{2}-\left|S_{1}\right|,\sigma_{1}\left[:s_{2}-s_{1}\right]+\sigma_{2}\right)$ if $s_{1} \leq s_{2} \leq \tilde{e}_{1} \leq e_{2}$
#  
# where $\tilde{e}_{1}=s_{1}+\left|\sigma_{1}\right|=e_{1}+\left|S_{1}\right|$.
# 
# After a few algebras, one can show that these four situations can be described by a unique combinations: 
# 
# \begin{multline}
# s_{1}<s_{2}\leq\tilde{e}_{1}\cup s_{2}\leq s_{1}\leq e_{2}\Rightarrow\\S_{2}\left(s_{2},e_{2},\sigma_{2}\right)S_{1}\left(s_{1},e_{1},\sigma_{1}\right)=S\left(s,e,\sigma_{1}\left[:s^{\prime}\right]+\sigma_{2}+\sigma_{1}\left[e^{\prime}:\right]\right)
# \end{multline}
# 
# with 
# 
# \begin{align}
# s&=\min\left(s_{1},s_{2}\right)\\e&=\max\left(\tilde{e}_{1},e_{2}\right)-\left|S_{1}\right|\\s^{\prime}&=\max\left(s_{2}-s_{1},0\right)\\e^{\prime}&=\min\left(e_{2}-s_{1},\left|\sigma_{1}\right|\right)
# \end{align}
# 
# The two situations $s_{1}<s_{2}\leq\tilde{e}_{1}$ and $s_{2}\leq s_{1}\leq e_{2}$ are called `<=` ($S_{1}$ overlaping on the left of $S_{2}$) and `>=` ($S_{1}$ overlaping on the right of $S_{2}$) in term of the `Substitution` object. The four situations `>`, `<`, `<=` and `>=` completely determine the different possibilities for two consecutive `Substitution` objects to be ordered.
# 
# Now we have all the algebra to combine a sequence, and to try to reduce it. The strategy is to capture the left-most substitution, supposed to be the one that has the minimum $s$ in the sequence, and to displace it at the begining of the sequence, permuting it with the non-overlaping substitution and combining it with the overlaping substitution during the displacement. That's all, and that's what the `SubstitutionSequence.compress` does. Nevertheless, du to the permutation of the objects without overlaping, it might be that the so-sorted `Substitution` are not in increasing order of their `start` position. So one has to recursively applies the `compress` method in order to be sure one has an ordered sequence at the end. In the same way, the last displacement of the `SubstitutionSequence.displace(start,end)` does not verify that the displaced element is not near a `Substitution` (already ordered) that still overlaps with it. So, one has to check this, do a displacement again, and launch again a compress method to verify all the machinery has been performed. At the end, this is not really costy in terms of recursivity.
# 
# **Important remark:** while it is custom in mathematics to put the first applied operator on the right of the sequence, Python reads sequence from left to right. So the first applied `Substitution` in a `SubstitutionSequence` is in fact the zero-th element `SubstitutionSequence[0]`.

# ## Verification of the `sort` method
# 
# To verify the compress method, one simply check that a randomly generated sequence of `Substitution`, once compressed, still transform a random sequence in the same way as its uncompressed version.
# 
# A entire sequence can be applied at once using the `SubstitutionSequence.apply(string)` method. In addition, one can ask for the method `SubstitutionSequence.are_overlaping(p,q)` with two consecutive integers `p` and `q` representing the position of the `Substitution` in the `SubstitutionSequence`.

# In[4]:


import random as r

from substitutionstring.substitution_sequence import SubstitutionSequence

def generate_long_sequence(N):
    string = "abcdefghijklmnopqrstuvwxyz0123456789"
    initial_string = ''.join(r.choices(string,k=550))
    sub_string, sequence = initial_string, SubstitutionSequence()
    for _ in range(N):
        start, end = r.randrange(0,len(sub_string)), r.randrange(0,len(sub_string))
        string_ = ''.join(r.choices(string,k=start))
        if end <= start: 
            start, end = end, start
        substitution = Substitution(start,end,string_)
        sequence.append(substitution)
        sub_string = substitution.apply(sub_string)
    return sequence, initial_string

# generate random sequence with N Substitution in it
for N in range(15,25):
    sequence, initial_string = generate_long_sequence(N)
    string1 = sequence.apply(initial_string)
    reordered_sequence = sequence.sort()
    string2 = reordered_sequence.apply(initial_string)
    # verify that the two SubstitutionSequence give the same string
    assert(string1==string2)
    # verify that the sorted SubstitutionSequence is indeed sorted
    for subst1,subst2 in zip(reordered_sequence[:-1],reordered_sequence[1:]):
        assert(subst1.start < subst2.start)
    # verify there is no overlapping Substitution anymore
    overlapping = [reordered_sequence.are_overlapping(p,p+1)
                   for p in range(len(reordered_sequence)-1)]
    assert(not any(overlapping))
print("no assert failed")


# In[5]:


from datetime import datetime
print("Last modification {}".format(datetime.now().strftime("%c")))

