---
sidebar_label: compression_tools
title: substitutionstring.compression_tools
---

Tools and algorithms that allows the compression of a sequence of Substitution objects.

All these methods are reported to the object SubstitutionSequence

## permute\_non\_overlapping

```python
permute_non_overlapping(substitution1, substitution2)
```

Permute two Substitution objects when they do not overlap. substitution1 and substitution2 must be in a sequence in that order, that is, substitution1 must be applied before substitution2 is performed. Then this function will flip these two substitutions in a sequence. Raises a ValueError in case the two substitutions overlap, in which case they should not be permuted, but might be combined.

## combine\_overlapping

```python
combine_overlapping(substitution1, substitution2)
```

Combine overlapping Substitution objects in a single Substitution object. The two Substitution objects substitution1 and substitution2 (the parameters) must be applied in that order, that is, substitution1 is applied (see apply_substitution function above) before substitutio2 is peformed. Returns a single Substitution object. Raises a ValueError in case the two Substitution objects are not overlapping.

## displace\_substitution\_in\_sequence

```python
displace_substitution_in_sequence(sequence, start, end)
```

Displace the Substitution object from positio start in the sequence to the position end in the sequence. If in the way one can combine two substitutions, one does this combination

## select\_minimal\_displacement

```python
select_minimal_displacement(sequence, already_displaced=list())
```

From a sequence, try all the combinations of displacements of Substitution objects, and returns the best displacement, that is, the displacement that produces the minimum memory size that the Substitution objects inside the sequence would represent.

To allow (artificially) memory to the Substitution objects we suppose that each character of the string is encoded in one digit, and each integer (start and end attributes) are also encoded in one digit. So we count the number of digits of each Substitution object, and we optimize locally the best displacement (with combination in case there is overlapping among different Substitution objects) while doing this displacement.

Parameters :
    - sequence: a list of Substitution objects
    - already_displaced: a list of tuples that one would prefer to remove from the combinations of displacements that this method generates during the computation. Recall that a displacement (see displace_substitution_in_sequence) consists in displacing the Substitution at the start position in the sequence to the end position inside the sequence.

Returns : 
    a tuple of integers that consists in 
    - the minimal size of cumulated strings that the sequence would produce
    - the start position of the Substitution object in the sequence
    - the end position of the Substitution object in the sequence

## compress\_sequence

```python
compress_sequence(sequence)
```

Compress the sequence so that the resulting one will have less Substitution objects inside, and the associated strings will be smaller than the actual ones.

