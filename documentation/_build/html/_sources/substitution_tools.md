---
sidebar_label: substitution_tools
title: substitutionstring.substitution_tools
---

Some methods that manipulate Substitution objects. These methods are all implemented (with quite similar names) as methods of the Substitution object. See its documentation for more details.

## substitute\_and\_revert

```python
substitute_and_revert(string, start, end, substitution_string)
```

Substitute the substitution_string in the string from position start to position end. That is, the resulting string will be string[:start]+substitution_string+string[end:]. In the same time, one constructs the Substitution object that can be fed to `apply_substitution` in order to revert the substitution.

## apply\_substitution

```python
apply_substitution(string, substitution)
```

Apply the process of a substitution (being a Substitution instance), by injecting a Substitution instance along a string.

## apply\_substitution\_string

```python
apply_substitution_string(string, substitution)
```

Apply the substitution (being a Substitution instance), and returns the substituted string.

## shift\_start\_end

```python
shift_start_end(start, end, substitution)
```

Find the new position start and end (seen as the index of string) once substitution is applied.

