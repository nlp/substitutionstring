---
sidebar_label: substitution_sequence
title: substitutionstring.substitution_sequence
---

While two `Substitution` objects do not commute in general, it is possible to manipulate several `Substitution` when they are supposed to apply in a sequence.

# SubstitutionSequence Objects

```python
class SubstitutionSequence()
```

`SubstitutionSequence` is a class that consists in a sequence of `Substitution` objects, and handles some basic manipulation of these `Substitution` inside the full sequence. 

For instance, `SubstitutionSequence` is able to combine several `Substitution` objects, provided they overlap among each other (vie the `displace(start,stop)` method). In addition, it has some properties of a sequence, like the possibility to concatenate several `SubstitutionSequence` into a bigger one (append, extend and + methods).

## \_\_init\_\_

```python
 | __init__(sequence: List[Substitution] = list())
```

Take as entry a list of `Substitution` objects.

## \_\_repr\_\_

```python
 | __repr__() -> str
```

Display the number of `Substitution` objects present into the `SubstitutionSequence`.

## \_\_reversed\_\_

```python
 | __reversed__() -> Iterator[Substitution]
```

Generate the `Substitution` inside the `SubstitutionSequence`, in the reverse order.

## \_\_eq\_\_

```python
 | __eq__(sequence: Type[SS])
```

Returns True if all `Substitution` in the given sequence are the same as in this one. Otherwise returns False.

## \_\_check\_substitution

```python
 | __check_substitution(substitution)
```

Raises a TypeError in case the object that one pass to this method is not a valid `Substitution` object.

## \_\_len\_\_

```python
 | __len__()
```

Returns the number of `Substitution` contained inside the `SubstitutionSequence`.

## \_\_getitem\_\_

```python
 | __getitem__(n: Union[int, slice]) -> Union[Type[SS],Substitution]
```

Returns the element (a `Substitution`) or a slice (a `SubstitutionSequence`), like a list.

## apply

```python
 | apply(string: str) -> str
```

Apply the complete sequence of Substitution to the given string. Raises an IndexError in case one of the Substitution can not be applied to the string during the process. Returns a string with all Substitution performed.

## append

```python
 | append(substitution: Substitution) -> None
```

Append a Substitution to the actual `SubstitutionSequence`. Returns None. Raises a TypeError in case the object one tries to extend is not a correct Substitution.

## extend

```python
 | extend(sequence: Type[SS]) -> None
```

Extend the `SubstitutionSequence` by the new one, in place. Returns None. Raises a TypeError in case the object one tries to extend is not a correct `SubstitutionSequence`.

## pop

```python
 | pop(n: int) -> None
```

Extract the element n from the `SubstitutionSequence`, and erase it from the sequence

## \_\_add\_\_

```python
 | __add__(sequence: Type[SS]) -> Type[SS]
```

Allows to concatenated several `SubstitutionSequence` into one bigger one. Returns a `SubstitutionSequence`.

## are\_overlapping

```python
 | are_overlapping(p: int, q: int) -> bool
```

If the `Substitution` at position `p` and position `q` overlap, returns True. Otherwise returns False.

Raises a ValueError in case `p` and `q` are not contiguous.

## \_permute\_non\_overlapping

```python
 | _permute_non_overlapping(p: int, q: int) -> Tuple[Substitution,Substitution]
```

Permute two `Substitution` objects when they do not overlap. Raises an IndexError in case the two substitutions overlap, in which case they should not be permuted, but might be combined.

## \_combine\_overlapping

```python
 | _combine_overlapping(p: int, q: int) -> Substitution
```

Combine overlapping `Substitution` objects in a single `Substitution` object. Returns a single `Substitution` object. Raises an IndexError in case the two `Substitution` objects are not overlapping.

## displace

```python
 | displace(start: int, end: int) -> Type[SS]
```

Displace Substitution at position start in the `SubstitutionSequence` to position end in the `SubstitutionSequence`. While doing so, if two Substitution overlap, they will be combined. Returns a `SubstitutionSequence`

## leftmost\_index

```python
 | leftmost_index() -> int
```

Find the minimum of the sequence, that is, the Substitution that starts at the lowest start position. Returns its index in the `SubstitutionSequence`.

## sort

```python
 | sort() -> Type[SS]
```

Try to order the `SubstitutionSequence` with left-most Substitution (defined as the one with the smallest start value) on the left, and in increasing order of the start value of the different Substitution inside the sequence. Due to permutation between non-overlapping Substitution, there might be some remaining element. Then the sort method applies recursively to avoid such situation. Returns an ordered `SubstitutionSequence`.

## \_remove\_empty\_on\_string

```python
 | _remove_empty_on_string(string: str = str()) -> Type[SS]
```

Apply the `SubstitutionSequence` to the given string, and if a `Substitution` equals its inverse, we drop this `Substitution` from the `SubstitutionSequence`. Recall that a `Substitution` equals its inverse when it does nothing.

## remove\_empty

```python
 | remove_empty(string: str = str()) -> Type[SS]
```

Remove empty Substitution inside the `SubstitutionSequence`. If a string is given, remove also the `Substitution` which are their own inverse. Return a new `SubstitutionSequence`.

