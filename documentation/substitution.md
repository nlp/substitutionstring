---
sidebar_label: substitution
title: substitutionstring.substitution
---

At a really basic level, the only modifications a string can tolerate are either
 - delete a part of the string from position `start` to position `end`
 - insert a `string` at position `start`

But these two modifications are in fact the two facets of a substitution, this later object having three attributes `start`, `end` and `string` since one has that
 - inserting a string into a string consists in susbstituting a new string when `start=end`, while
 - deleting a string consists in substituting an empty string between different `start` and `end` positions

By itself, a `Substitution` has no real meaning, since it does not handle the state of the string before and after the Substitution is performed. Then, one has to collect either: 
 1) the initial state of the string and the sequence of all the `Substitution` this string undergoes
or
 2) the final state of the string and the sequence of all the `Substitution` this string can undergo to recover its initial state

The second case is automatically handled by the object `SubstitutionString` somewhere else in this package. The first case can be recovered from the `SubstitutionString` object by using its `reversed` method.

# Substitution Objects

```python
class Substitution()
```

Represent the parameters of a substitution in a string, that is
 - `start` : the position into a given string where the deletion starts,
 - `end` : the position of a given string where the deletion ends (thus the part `string[start:end]` will be removed),
 - `string` : the string which will be inserted between start and end. Do not confound the `string` contained in this argument (which can be thought as a children string), and the string onto which the `Substitution` might apply (and can be thought as a parent string).

The `Substitution` class has been designed to work specifically in some reversible cleaning objects as a container for later uses. 

See elsewhere the class `SubstitutionString` for instance.

## \_\_init\_\_

```python
 | __init__(start: int = 0, end: int = 0, string: str = str()) -> None
```

Parameters | Type | Details
-- | -- | --
`start` | integer | The position where the deletion in the parent string will start if method `Substitution.apply(string)` is called. Default is `0`
`end` | integer | The position where the deletion in the parent string will end if method `Substitution.apply(string)` is called. Default is `0`.
`string` | string | The string that will be inserted in between the `start` and `end` positions of the parent string, if method `Substitution.apply(string)` is called. Default is the empty string.

All these parameter become attributes of the `Substitution` instance.

Returns `None`.

## \_\_len\_\_

```python
 | __len__() -> int
```

Returns the length (an integer) of the `Substitution.string` attribute.

## \_\_abs\_\_

```python
 | __abs__() -> int
```

Returns the change of length of the parent string if method `Substitution.apply(string)` is called. Note that this value can be negative if the inserted string is smaller than the deleted one.

Examples : 
```python3
abs(Substitution(3,5,'123456')) = 4
abs(Substitution(3,9,'123')) = -3
```

## \_\_repr\_\_

```python
 | __repr__() -> str
```

Displays the three attributes of the `Substitution` class.

## \_\_eq\_\_

```python
 | __eq__(substitution: Type[S]) -> bool
```

Parameters | Type | Details
-- | -- | --
`substitution` | `Substitution` instance | The `Substitution` instance to be compared with the actual one.

Returns | Type | Details
-- | -- | --
`bools` | boolean | Returns True if all `start`, `end` and `string` arguments are equal among the two `Substitution` instances. Otherwise returns False. 

Raises | Details
-- | -- 
AttributeError | In case `substitution` is not a valid `Substitution` instance.

## \_\_gt\_\_

```python
 | __gt__(substitution: Type[S]) -> bool
```

Parameters | Type | Details
-- | -- | --
`substitution` | `Substitution` instance | The `Substitution` instance to be compared with the actual one.

Returns | Type | Details
-- | -- | --
`bools` | boolean | Returns `True` when `substitution` is completely on the left of `Substitution` without overlapping. Otherwise returns `False`. 

Raises | Details
-- | -- 
AttributeError | In case `subst` is not a valid `Substitution` instance (at least it has no `end` attribute).

`Substitution` is supposed to apply prior to `substitution` for the notion of overlapping to be make sense, and the two `Substitution` should be consecutive in a sequence of `Substitution`, applied to a given parent string. These conditions can not be verified at the level of the `Substitution` object.

## \_\_ge\_\_

```python
 | __ge__(substitution: Type[S]) -> bool
```

Parameters | Type | Details
-- | -- | --
`substitution` | `Substitution` instance | The `Substitution` instance to be compared with the actual one.

Returns | Type | Details
-- | -- | --
`bools` | boolean | `True` when `substitution` partially overlaps on the left of the `Substitution` object. Otherwise `False`.  

Raises | Details
-- | -- 
AttributeError | In case `substitution` is not a valid `Substitution` instance (neither `start` nor `end` attribute, or at least of them absent).

`Substitution` is supposed to apply prior to `substitution` for the notion of overlapping to be make sense, and the two `Substitution` should be consecutive in a sequence of `Substitution`, applied to a given parent string. These conditions can not be verified at the level of the `Substitution` object.

## \_\_lt\_\_

```python
 | __lt__(substitution: Type[S]) -> bool
```

Parameters | Type | Details
-- | -- | --
`substitution` | `Substitution` instance | The `Substitution` instance to be compared with the actual one.

Returns | Type | Details
-- | -- | --
`bools` | boolean | `True` when `substitution` is completely on the irght of the `Substitution` object without overlapping. Otherwise `False`.  

Raises | Details
-- | -- 
AttributeError | In case `substitution` is not a valid `Substitution` instance (at least it has no `start` attribute).

`Substitution` is supposed to apply prior to `substitution` for the notion of overlapping to be make sense, and the two `Substitution` should be consecutive in a sequence of `Substitution`, applied to a given parent string. These conditions can not be verified at the level of the `Substitution` object.

## \_\_le\_\_

```python
 | __le__(substitution: Type[S]) -> bool
```

Parameters | Type | Details
-- | -- | --
`substitution` | `Substitution` instance | The `Substitution` instance to be compared with the actual one.

Returns | Type | Details
-- | -- | --
`bools` | boolean | `True` when `substitution` partially overlaps on the right of the `Substitution` object. Otherwise `False`.  

Raises | Details
-- | -- 
AttributeError | In case `substitution` is not a valid `Substitution` instance (at least it has no `start` attribute).

`Substitution` is supposed to apply prior to `substitution` for the notion of overlapping to be make sense, and the two `Substitution` should be consecutive in a sequence of `Substitution`, applied to a given parent string. These conditions can not be verified at the level of the `Substitution` object.

## \_\_check\_start\_end\_string

```python
 | __check_start_end_string(string: str) -> None
```

Check whether the parameters of the `Substitution` instance are compatible with the given `string`. Raises an IndexError in case they are not compatible.

## apply

```python
 | apply(string: str) -> str
```

Apply the `Substitution` to a given parent`string`. Returns the string after the substitution is performed. Raises an IndexError in case the Substitution and the string are not compatible.

Parameters | Type | Details
-- | -- | --
`string` | a string | The parent string onto which the `Substitution` will be applied.

Returns | Type | Details
-- | -- | --
`string` | string | The state of the parent string once the substitution is done.

Raises | Details
-- | -- 
IndexError | In case `Substitution` is not compatible with the parent string. For instance if `Substitution.end` is larger than the length of the input `string`.

## \_\_call\_\_

```python
 | __call__(string: str) -> str
```

Alias for `Substitution.apply(string)`. Returns a string.

## revert

```python
 | revert(string: str) -> Type[S]
```

Reverts the `Substitution` object that one can apply to the modified string in order to recover the initial one.

Parameters | Type | Details
-- | -- | --
`string` | a string | The parent string onto which the `Substitution` should be applied. The `Substitution` will not be applied, but its inverse will be calculated and returned.

Returns | Type | Details
-- | -- | --
`substitution` | `Substitution` instance | The `Substitution` that should be applied to `Substitution.apply(string)` in order to recover the initial string.

**Example**:

```python3
string = '0123456789'
subst = Substitution(3,5,'abc')
sub_string = subst.apply(string) # returns '012abc56789'
revertSubst = subst.revert(string)
revertSubst.apply(sub_string) # returns '0123456789'
```

## apply\_and\_revert

```python
 | apply_and_revert(string: str) -> Tuple[str, Type[S]]
```

Perform both the `Substitution.apply` and the `Substitution.revert` onto a string. Returns the string and the `Substitution` object, in that order.

Parameters | Type | Details
-- | -- | --
`string` | a string | The parent string onto which the `Substitution` will be applied.

Returns | Type | Details
-- | -- | --
`string` | a string | The state of the string after the `Substitution` is performed.
`substitution` | `Substitution` instance | The `Substitution` that should be applied to `Substitution.apply(string)` in order to recover the initial string.

Examples : 
```python3
string = '0123456789'
subst = Substitution(3,5,'abcde')
string_, subst_ = subst.apply_and_revert(string)
# string_ is '012abcde56789'
# subst_ is Substitution(3, 8, '34')
subst_.apply(string_) # gives back the string '0123456789'
```

Note that an `apply_and_revert` applied twice give back the initial objects.

```python3
string0 = '0123456789'
subst0 = Substitution(3,5,'abcde')
string1, subst1 = subst0.apply_and_revert(string0)
string2, subst2 = subst1.apply_and_revert(string1)
assert(string2==string0)
assert(subst2==subst0)
```

## shift\_start\_end

```python
 | shift_start_end(start: int, end: int) -> Tuple[int, int]
```

Find the new position `start` and `end` (seen as the index of a string) as if `Substitution` was not applied. This method allows to transfer the position of a token throught the different `Substitution` objects of a sequence. It is useful for the `restore` method of a `SubstitutionString` instance.

Parameters | Type | Details
-- | -- | --
`start` | int | The initial position inside a string that one wants to shift by the `Substitution`
`end` | int | The final position inside a string that one wants to shift by the `Substitution`.

Returns | Type | Details
-- | -- | --
`start` | int | The shifted initial position.
`end` | int| The shifted final position.

**Examples**:

```
python
string0 = '0123456789'
subst0 = Substitution(3,5,'abcde')
string1, subst1 = subst0.apply_and_revert(string0)

# string1[3:8] is 'abcde'
start0, end0 = subst1.shift_start_end(3,8) # returns 3,5
# string0[start0:end0] is '34', the deleted part of string0 by subst0

# string0[3:5] is '34', the deleted part of string0 by subst0
start1, end1 = subst0.shift_start_end(3,5) # returns 3,8
# string1[3:8] is 'abcde', the inserted part of string1 from string0 by subst0
```

