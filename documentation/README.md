# Introduction and basic example

The `substitutionstring` package contains three main classes : 
 - `Substitution`, that can be seen as a container object dealing with the notion of a substitution of a children string into parent string, and a few machinery (like `apply` and `revert`) that can be constructed once applying the `Substitution` operator to a given string.
 - `SubstitutionSequence`, that can be seen as a sequence of several `Substitution` operators applied in a raw. this class deals with the compression (a try to make the complete sequence memory efficient), and the displacement of the `Substitution` operators inside the sequence.
 - `SubstitutionString`, that allows manipulation of a string in a rather simple way (using `insert`, `delete`, `substitute` and `sub` operations for inserting, deleting, substituting, and applying substitution from REGular Expressions/REGEX), without loss of information -- that is, any modification of the initial string can be reverted. In particular, this class allows to reconstruct the initial sub-string that ends up in the range `[s:e]` after one applied many transformations to the initial string.

## Basic example of `SubstitutionString`

The simplest way to use this package is through the `SubstitutionString` class. Below is a basic example of its usage.


```python3
from substitutionstring import SubstitutionString

string = 'test of a string'
substring = SubstitutionString(string=string)

substring.insert(5,'new insert ') 
# insert string 'new insert ' at position 5 of the previous one
# # 'test new insert of a string'

substring.substitute(9,15,'substitution') 
# delete the previous string in the range [9:15] and 
# substitute the string 'substitution'
# # 'test new substitution of a string'

substring.delete(9,21) # delete the previous string from range [9:21]
# # 'test new  of a string'

substring.sub(r'\s{2,}',' ') 
# substitute all spaces larger than 2 by a single one. Any REGEX is accepted.
# # 'test new of a string'

substring.sequence 
# list of Substitution objects that are collected into a SubstitutionSequence
# # SubstitutionSequence(4 Substitutions)
# one can think of a SubstitutionSequence as a list of Substitution
for substitution in substring.sequence:
    print(substitution)
# # returns
# Substitution(start=5, end=16, string=``)
# Substitution(start=9, end=21, string=`insert`)
# Substitution(start=9, end=9, string=`substitution`)
# Substitution(start=8, end=9, string=`  `)

# what is recorded is the inverse Substitution at each step. 
# For instance, to revert the insertion of 'new_insert ' (or length 11) from
# position 5 (the first invert applied), one has to delete the string from
# position 5 to 16 in the new modified string.

substring.revert() # revert the previous step
# # 'test new  of a string'

len(substring) # length of the pipeline list
# # 3

substring.revert(len(substring)) # revert to the intial string
# # 'test of a string'
```

The other classes are detailled in the following pages.