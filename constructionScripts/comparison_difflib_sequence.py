#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
One compares the result of the compression of a sequence with the outcome of the longest common substring (LCS) as calculated via the difflib library.
"""
import random as r
from difflib import SequenceMatcher

from substitutionstring import Substitution, SubstitutionSequence
from substitutionstring import compression_tools as compress

def construct_sequence_from_diff(a,b):
    """Constructs the sub-blocks in common from the a and b sequences, and then constructs the pipeline that can pass from sequence a to sequence b using Substitution. Returns the pipeline, that is, the list of Substitution objects."""
    seq_matcher = SequenceMatcher(a=a,b=b,autojunk=False)
    blocks = seq_matcher.get_matching_blocks()
    sequence = SubstitutionSequence()
    start_a, shift_a, start_b = 0, 0, 0
    for match in blocks:
        substitution = Substitution(start=start_a, end=match.a+shift_a,
                                    string=b[start_b:match.b])
        shift_a += abs(substitution)
        start_a = match.a + match.size + shift_a
        start_b = match.b + match.size
        sequence.append(substitution)
    sequence = sequence.remove_empty()
    return sequence

def cost(sequence):
    return sum(len(subst) + (1 if subst.start==subst.end else 2)
               for subst in sequence) 

def generate_long_sequence(N):
    string = "abcdefghijklmnopqrstuvwxyz0123456789"
    initial_string = ''.join(r.choices(string,k=50))
    sub_string, sequence = initial_string, SubstitutionSequence()
    for _ in range(N):
        start, end = r.randrange(0,len(sub_string)), r.randrange(0,len(sub_string))
        string_ = ''.join(r.choices(string,k=start))
        if end <= start: 
            start, end = end, start
        substitution = Substitution(start,end,string_)
        sequence.append(substitution)
        sub_string = substitution.apply(sub_string)
    return sequence, initial_string, sub_string

def print_position(n):
    i, s = 0, ''
    while len(s)<n:
        s += str(i)+'123456789'
        i += 1
    return print(s)

# are all compressed sequence of length 1 ? NO, but they are scarce
for N in range(15,25):
    sequence, initial_string, final_string = generate_long_sequence(N)
    string1 = sequence.apply(initial_string)
    reordered_sequence = sequence.compress()
    if len(reordered_sequence)>1:
        print("STOP")
        break
    string2 = reordered_sequence.apply(initial_string)
    assert(string1==string2)
    for subst1,subst2 in zip(reordered_sequence[:-1],reordered_sequence[1:]):
        assert(subst1.start < subst2.start)
overlapping = [reordered_sequence.are_overlapping(p,p+1)
               for p in range(len(reordered_sequence)-1)]
print(any(overlapping))

compress_sequence = compress.compress_pipeline(sequence)
for subst in compress_sequence:
    print(subst)
for subst in reordered_sequence:
    print(subst)
for subst in sequence:
    print(subst)
seq_matcher = SequenceMatcher(a=initial_string,b=string1,autojunk=False)
blocks = seq_matcher.get_matching_blocks()
blocks

sequence = construct_sequence_from_diff(initial_string,string1)
compress_sequence = sequence.compress()

string3 = compress_sequence.apply(initial_string)
string3 == string1

cost(sequence)
cost(compress_sequence)
cost(reordered_sequence)

print(initial_string)
print_position(len(initial_string))
print(string1)
print_position(len(string1))

# are all longest common substring impossible to compress ? seems YES
# but they are not always the best memory choice

costs_lower, costs_equal, costs_higher = [], [], []
for _ in range(100):
    sequence, initial_string, final_string = generate_long_sequence(25)
    block_seq = construct_sequence_from_diff(initial_string,final_string)
    block_seq_compress = block_seq.compress()
    if len(block_seq_compress)!=len(block_seq):
        print("STOP")
        break
    sequence_compress = sequence.compress()
    c1 = cost(sequence_compress)
    c2 = cost(block_seq_compress)
    if c1 > c2:
        costs_lower.append(c1-c2)
    elif c1==c2:
        costs_equal.append(0)
    elif c1 < c2:
        costs_higher.append(c2-c1)
print("LCS costs the same as compression for {} cases".format(len(costs_equal)))
print("LCS costs higher than compression for {} cases".format(len(costs_higher)))
print("LCS costs lower than compression for {} cases".format(len(costs_lower)))
    


# are all longest common substring only contains deletion and insertion ? 
# NOT at all, it's even an exception

exceptions = []
for _ in range(100):
    sequence, initial_string, final_string = generate_long_sequence(25)
    block_seq = construct_sequence_from_diff(initial_string,final_string)
    block_seq_compress = block_seq.compress()
    if len(block_seq_compress)!=len(block_seq):
        print("STOP")
        break
    flag, i = True, 0
    while i<len(block_seq_compress) and flag:
        subst = block_seq_compress[i]
        if subst.start != subst.end and bool(subst.string):
            exceptions.append((initial_string,final_string))
            flag = False
        i += 1
len(exceptions)

n = 0
block_seq = construct_sequence_from_diff(*exceptions[n])
for s in block_seq: print(s)
check_string = block_seq.apply(exceptions[n][0])
assert(check_string == exceptions[n][1])
print("-"*12)
block_seq_compress = block_seq.compress()
for s in block_seq_compress: print(s)
print("<>"*6)
print(exceptions[n][0])
print("-"*12)
print(exceptions[n][1])
n += 1
