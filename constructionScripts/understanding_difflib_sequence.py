#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
One tries to understand the differences bewteen the LCS and the compression of a SubstitutionSequence
"""
import random as r
from difflib import SequenceMatcher

from substitutionstring import Substitution, SubstitutionSequence
from substitutionstring import compression_tools as compress

def construct_sequence_from_diff(a,b):
    """Constructs the sub-blocks in common from the a and b sequences, and then constructs the pipeline that can pass from sequence a to sequence b using Substitution. Returns the pipeline, that is, the list of Substitution objects."""
    seq_matcher = SequenceMatcher(a=a,b=b,autojunk=False)
    blocks = seq_matcher.get_matching_blocks()
    sequence = SubstitutionSequence()
    start_a, shift_a, start_b = 0, 0, 0
    for match in blocks:
        substitution = Substitution(start=start_a, end=match.a+shift_a,
                                    string=b[start_b:match.b])
        shift_a += abs(substitution)
        start_a = match.a + match.size + shift_a
        start_b = match.b + match.size
        sequence.append(substitution)
    sequence = sequence.remove_empty()
    return sequence

def cost(sequence):
    return sum(len(subst) + (1 if subst.start==subst.end else 2)
               for subst in sequence) 

def generate_sequence(nb_substitutions=15, length_initial_string=50):
    """Returns SubstitutionSequence, the initial string, and the final string."""
    string = "abcdefghijklmnopqrstuvwxyz0123456789"
    initial_string = ''.join(r.choices(string,k=length_initial_string))
    sub_string, sequence = initial_string, SubstitutionSequence()
    for _ in range(nb_substitutions):
        start, end = r.randrange(0,len(sub_string)), r.randrange(0,len(sub_string))
        string_ = ''.join(r.choices(string,k=start))
        if end <= start: 
            start, end = end, start
        substitution = Substitution(start,end,string_)
        sequence.append(substitution)
        sub_string = substitution.apply(sub_string)
    return sequence, initial_string, sub_string

def print_position(n):
    """Prints a series of 123456789 with intertwinned unit 0,1,2,3, as decades"""
    i, s = 0, ''
    while len(s)<n:
        s += str(i)+'123456789'
        i += 1
    return print(s)

def print_sequence(sequence,string):
    sub_string = string
    for s in sequence:
        print(s)
        print(sub_string)
        print_position(len(sub_string))
        sub_string = s.apply(sub_string)
        print(sub_string)
        print_position(len(sub_string))
        print("-"*12)
    return None

sequence, initial_string, final_string = generate_sequence(6,25)
while len(sequence.compress())<=1:
    sequence, initial_string, final_string = generate_sequence(3,15)
difflib_sequence = construct_sequence_from_diff(initial_string, final_string)

print(initial_string)
print_position(len(initial_string))
print(final_string)
print_position(len(final_string))

print_sequence(sequence,initial_string)

print_sequence(sequence.compress(),initial_string)

print_sequence(difflib_sequence, initial_string)

cost(sequence.compress())
cost(difflib_sequence)


substitution = sequence.compress()[-1]
substitution_ = substitution.revert(initial_string)
substitution_.apply(final_string) == initial_string

common_letters = [i for i in substitution.string 
                  if i in initial_string and i in final_string]
#%% start from the common letters, and then apply Substitution in between

common_letters = {i for i in initial_string if i in final_string}

def letter_positions(string,chars):
    """Get the positions of all chars in string"""
    letter_positions = {}
    for i,char in enumerate(string):
        if char in chars:
            try:
                letter_positions[char].append(i)
            except KeyError:
                letter_positions[char] = [i,]
    return letter_positions

def position_letters(letter_positions):
    position_letters = [(position,char) for char,positions in letter_positions.items()
                         for position in positions]
    return sorted(position_letters, key=lambda x: x[0])

initial_positions = letter_positions(initial_string, common_letters)
final_positions = letter_positions(final_string, common_letters)
initial_letters = position_letters(initial_positions)
final_letters = position_letters(final_positions)

initial_common = ''.join(x[1] for x in initial_letters)
final_common = ''.join(x[1] for x in final_letters)
print(initial_common)
print(final_common)

problematic_chars_cardinal = {char:len(final_positions[char])-len(initial_positions[char])
                              for char in common_letters 
                              if len(final_positions[char])-len(initial_positions[char]) != 0}
problematic_char_position = None



#%%

seq_test12 = SubstitutionSequence([substitution,substitution_])
seq_test12.displace(0,1)[0]
seq_test21 = SubstitutionSequence([substitution,substitution_])
seq_test21.displace(1,0)[0]

permuted_sequence = sequence.displace(0,1)
print_sequence(permuted_sequence, initial_string)

Sp = permuted_sequence[-2]
Sq = permuted_sequence[-1]

for subst in self:
    print(subst)
    print(not(subst.start==subst.end and not len(subst)))


#%% check of the complexity of in

string = "abcdefghijklmnopqrstuvwxyz0123456789"
from time import time

times = []
for length_string in range(500,5000):
    s1 = ''.join(r.choices(string,k=length_string))
    s2 = ''.join(r.choices(string,k=length_string))
    t0 = time()
    common_letters = [i for i in s1 if i in s2]
    t0 = time() - t0
    times.append(t0)

difference = [t1-t0 for t1,t0 in zip(times[:-1],times[1:])]
    
import matplotlib.pyplot as plt
