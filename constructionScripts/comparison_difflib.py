#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from difflib import SequenceMatcher
import random as r

from stringsubstitution.substitution import Substitution
from stringsubstitution.substitution_string import SubstitutionString
from stringsubstitution.compression_tools import compress_pipeline

string = "abcdefg" # + "hijklmnopqrstuvwxyz     0123456789"

def construct_pipeline_from_diff(a,b):
    """Constructs the sub-blocks in common from the a and b sequences, and then constructs the pipeline that can pass from sequence a to sequence b using Substitution. Returns the pipeline, that is, the list of Substitution objects."""
    seq_matcher = SequenceMatcher(a=a,b=b,autojunk=False)
    blocks = seq_matcher.get_matching_blocks()
    pipeline = []
    start_a, shift_a, start_b = 0, 0, 0
    for match in blocks:
        substitution = Substitution(start=start_a, end=match.a+shift_a,
                                    string=b[start_b:match.b])
        shift_a += abs(substitution)
        start_a = match.a + match.size + shift_a
        start_b = match.b + match.size
        pipeline.append(substitution)
    return pipeline

problems = []
for _ in range(50):
    a = ''.join(r.choices(string,k=25))
    b = ''.join(r.choices(string,k=35))
    pipeline = construct_pipeline_from_diff(a,b)
    subString = SubstitutionString(a)
    subString.apply_pipeline(pipeline)
    if subString.string != b:
        problems.append((a,b,pipeline))
print(len(problems))

problems, good_compressions = [], []
for _ in range(50):
    a = ''.join(r.choices(string,k=25))
    b = ''.join(r.choices(string,k=35))
    pipeline = construct_pipeline_from_diff(a,b)
    min_pipeline = compress_pipeline(pipeline)
    subString = SubstitutionString(a)
    subString.apply_pipeline(pipeline)
    if subString.string != b:
        problems.append((a,b,pipeline))
        
    subString = SubstitutionString(a)
    subString.apply_pipeline(min_pipeline)
    if subString.string != b:
        problems.append((a,b,min_pipeline))
        
    if len(min_pipeline)<len(pipeline):
        good_compressions.append((pipeline,min_pipeline))
print(len(problems))
print(len(good_compressions))

a = ''.join(r.choices(string,k=25))
b = ''.join(r.choices(string,k=35))
pipeline_a2b = construct_pipeline_from_diff(a,b)
subString_a = SubstitutionString(a)
subString_a.apply_pipeline(pipeline_a2b)
subString_a.string==b

pipeline_b2a = list(reversed(subString_a.pipeline))
subString_b = SubstitutionString(b)
subString_b.apply_pipeline(pipeline_b2a)
subString_b.string==a

pipeline_b2a == construct_pipeline_from_diff(b,a) # return False

pipeline_b2a = construct_pipeline_from_diff(b,a)
subString_b = SubstitutionString(b)
subString_b.apply_pipeline(pipeline_b2a)
subString_b.string==a
