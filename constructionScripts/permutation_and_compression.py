#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random as r

from iamcleaning.substitution import Substitution
from iamcleaning.substitution_string import SubstitutionString
from iamcleaning import compression_tools as compress

string = "abcdefghijklmnopqrstuvwxyz     0123456789"

rstring = ''.join(r.choices(string,k=550))
sstring = SubstitutionString(rstring)

N = r.randrange(3,10)
for _ in range(N):
    start, end = r.randrange(0,len(sstring.string)), r.randrange(0,len(sstring.string))
    string_ = ''.join(r.choices(string,k=r.randrange(2,25)))
    if end <= start: 
        start, end = end, start
    sstring.substitute(start,end,string_)

pipeline = list(reversed(sstring))
len(pipeline)

test_string = rstring
for subst in pipeline:
    test_string = t.apply_substitution_string(test_string, subst)
print(test_string==sstring.string)

combinaisons = [(i,j) for j in range(N) for i in range(N) if i<j]

for comb in combinaisons:
    pipeline_ = t.displace_substitution_in_pipeline(pipeline, comb[0], comb[1])
    test_string = rstring
    for subst in pipeline_:
        test_string = t.apply_substitution_string(test_string, subst)
    print(comb,test_string==sstring.string,len(pipeline_))

combinaisons = [(i,j) for j in range(N) for i in range(N) if i>j]

for comb in combinaisons:
    pipeline_ = t.displace_substitution_in_pipeline(pipeline, comb[0], comb[1])
    test_string = rstring
    for subst in pipeline_:
        test_string = t.apply_substitution_string(test_string, subst)
    print(comb,test_string==sstring.string,len(pipeline_))

combinaisons = [(i,j) for j in range(N) for i in range(N)]

problems = []
for comb in combinaisons:
    pipeline_ = t.displace_substitution_in_pipeline(pipeline, comb[0], comb[1])
    test_string = rstring
    for subst in pipeline_:
        test_string = t.apply_substitution_string(test_string, subst)
    if test_string!=sstring.string:
        problems.append(comb)
print(len(problems))

min_pipeline = t.compress_pipeline(pipeline)
test_string = rstring
for subst in min_pipeline:
    test_string = t.apply_substitution_string(test_string, subst)
print(test_string==sstring.string)

def generate_pipeline(string):
    sstring = SubstitutionString(string)
    N = r.randrange(3,10)
    for _ in range(N):
        start, end = r.randrange(0,len(sstring.string)), r.randrange(0,len(sstring.string))
        string_ = ''.join(r.choices(string,k=r.randrange(2,25)))
        if end <= start: 
            start, end = end, start
        sstring.substitute(start,end,string_)
    
    pipeline = list(reversed(sstring))
    return pipeline, sstring.string

def pipeline_string_length(pipeline):
    return sum(len(subst) for subst in pipeline)

compressions, problems = [], []
for _ in range(500):
    pipeline, modified_string = generate_pipeline(rstring)
    l0 = pipeline_string_length(pipeline)
    try:
        min_pipeline = t.compress_pipeline(pipeline)
    except ValueError:
        problems.append(pipeline)
    l1 = pipeline_string_length(min_pipeline)
    compressions.append(l1/l0)
sum(compressions)/len(compressions)

problems = []
for _ in range(500):
    pipeline , modified_string= generate_pipeline(rstring)
    combinaisons = [(i,j) for j in range(len(pipeline)) 
                    for i in range(len(pipeline))]
    for comb in combinaisons:
        pipeline_ = t.displace_substitution_in_pipeline(pipeline, comb[0], comb[1])
        test_string = rstring
        for subst in pipeline_:
            test_string = t.apply_substitution_string(test_string, subst)
        if test_string!=modified_string:
            problems.append(comb)
print(len(problems))

