# -*- coding: utf-8 -*-

# IAM Cleaner Tests

To run all the tests, use

```bash
python3 -m unittest -v test/*.py
```

from the root of the package (one level above the `test/` forlder).

Last version : August, 2021
